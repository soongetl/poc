<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\DataPropertyInterface;
use Soong\Data\Property;

/**
 * Transformer to add 1 to the extracted data.
 *
 * @package Soong\Transformer
 */
class Increment implements TransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface
    {
        // @todo Don't use concrete class
        return new Property($data->getValue() + 1);
    }
}
