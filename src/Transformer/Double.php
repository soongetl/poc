<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\DataPropertyInterface;
use Soong\Data\Property;

/**
 * Transformer to multiply the extracted data value by 2.
 *
 * @package Soong\Transformer
 */
class Double implements TransformerInterface
{

    /**
     * {@inheritdoc}
     */
    public function transform(array $configuration, ?DataPropertyInterface $data) : ?DataPropertyInterface
    {
        // @todo Don't use concrete class
        return new Property(2 * $data->getValue());
    }
}
