<?php
declare(strict_types=1);

namespace Soong\KeyMap;

/**
 * A basic key map implementation for test/demonstration purposes.
 *
 * This does not persist, thus is not useful for real use cases.
 *
 * @package Soong\KeyMap
 */
class MemoryKeyMap extends KeyMapBase
{

    /**
     * The key mappings.
     *
     * @var array
     *   List, keyed by a hash of the extracted keys, of arrays containing:
     *     CONFIGURATION_EXTRACTOR_KEYS => extracted key
     *     CONFIGURATION_LOADER_KEYS => loaded key
     */
    protected $keys = [];

    /**
     * {@inheritdoc}
     */
    public function saveKeyMap(array $extractedKey, array $loadedKey) : void
    {
        $hash = $this->hashKeys($extractedKey);
        $this->keys[$hash] = [
          self::CONFIGURATION_EXTRACTOR_KEYS => $extractedKey,
          self::CONFIGURATION_LOADER_KEYS => $loadedKey,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function lookupLoadedKey(array $extractedKey) : array
    {
        $hash = $this->hashKeys($extractedKey);
        if (isset($this->keys[$hash])) {
            return $this->keys[$hash][self::CONFIGURATION_LOADER_KEYS];
        } else {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function lookupExtractedKeys(array $loadedKey) : array
    {
        foreach ($this->keys as $keyData) {
            if ($keyData[self::CONFIGURATION_LOADER_KEYS] == $loadedKey) {
                return $keyData[self::CONFIGURATION_EXTRACTOR_KEYS];
            }
        }
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(array $extractedKey) : void
    {
        $hash = $this->hashKeys($extractedKey);
        unset($this->keys[$hash]);
    }

    /**
     * {@inheritdoc}
     */
    public function count() : int
    {
        return count($this->keys);
    }

    /**
     * {@inheritdoc}
     */
    public function iterate() : iterable
    {
        foreach ($this->keys as $keyData) {
            yield $this->keys[self::CONFIGURATION_EXTRACTOR_KEYS];
        }
    }
}
