<?php
declare(strict_types=1);

namespace Soong\StatusLogger;

use Monolog\Logger;

/**
 * TBD: Handle logging of task statuses.
 *
 * @package Soong\StatusLogger
 */
class StatusLogger extends Logger implements StatusLoggerInterface
{

}
