# poc

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

A couple of simple demos of the Soong ETL ecosystem.

## Structure


```  
config/ - YAML files with configuration for ETL processes.
data/ - Sample source data for ETL migrations.
src/ - Some custom ETL components for demonstration purposes.
```


## Install

Via Composer

``` bash
$ composer require soong/poc
```

## Usage

To setup for testing Soong ETL:

1. Create an empty database for testing.
1. Import data/extractsource.sql to the database (table to be populated by the first demo).
1. Import data/beer.sql to the database (tables to be populated for the second demo).
1. Edit each of the `.yml` files in `config/` - where indicated, replace the sample credentials with those for the test database.

Demo 1:

1. Execute `vendor/bin/soong migrate arraytosql`
1. Look at the `extractsource` table to see the data populated.
1. Look at the `map_arraytosql` table to see the mapping from source to destination keys (identical in this case).
1. Execute `vendor/bin/soong migrate sqltocsv`
1. Observe CSV data output to the terminal with configured transformations applied.

Demo 2:

1. Execute `vendor/bin/soong migrate beertopics`
1. Observe the `beer_terms` table is populated from CSV data - in particular, see how the 'red ale' reference to its 'ale' parent has been converted to the numeric ID assigned to the 'ale' row in the database.
1. Execute `vendor/bin/soong migrate beeraccounts`
1. Observe the `beer_users` table - in particular, see how the `ValueLookup` transformer converted the boolean values in the `pro` column to strings in the `taster` column.
1. Execute `vendor/bin/soong migrate beercontent`
1. Observe the `beer` table - in particular, see how the relationships to users/accounts was maintained even though the IDs for the users changed (also see the `map_beeraccounts` table).
1. Execute `vendor/bin/soong rollback beercontent`
1. Observe how the `beer` and `map_beercontent` tables are now empty.

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email `soong@virtuoso-performance.com` instead of using the issue tracker.

## Credits

- [Mike Ryan][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/soong/poc.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/soong/poc/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/soong/poc.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/soong/poc.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/soong/poc.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/soong/poc
[link-travis]: https://travis-ci.org/soong/poc
[link-scrutinizer]: https://scrutinizer-ci.com/g/soong/poc/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/soong/poc
[link-downloads]: https://packagist.org/packages/soong/poc
[link-author]: https://gitlab.com/mikeryan776
[link-contributors]: ../../contributors
